class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.integer :processing_time
      t.integer :earliest_end
      t.integer :latest_end

      t.timestamps null: false
    end
  end
end
