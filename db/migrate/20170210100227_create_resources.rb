class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.string :name
      t.float :capacity
      t.float :overtime_cost

      t.timestamps null: false
    end
  end
end
