class AddResourceIdToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :resource_id, :integer
  end
end
