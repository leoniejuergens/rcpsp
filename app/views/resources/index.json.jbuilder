json.array!(@resources) do |resource|
  json.extract! resource, :id, :name, :capacity, :overtime_cost
  json.url resource_url(resource, format: :json)
end
