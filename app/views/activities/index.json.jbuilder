json.array!(@activities) do |activity|
  json.extract! activity, :id, :name, :processing_time, :earliest_end, :latest_end
  json.url activity_url(activity, format: :json)
end
